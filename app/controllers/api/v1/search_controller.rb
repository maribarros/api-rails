module Api
  module V1
    class SearchController < ApplicationController
      before_action :min_json

      def index
        render json: { result: min_json }
      end

      private

      def json_result
        SearchRepositoryService.execute(
          params[:query]
        )
      end

      def min_json
        array_result = JSON.parse(json_result)
        json_result = []
        array_result['items'].each do |json|
          json_result << json_item(json)
        end
        json_result
      end

      def is_ruby?
        item['language'] == 'ruby'
      end

      def json_item(item)
        {
          autor: item['name'],
          full_name: item['full_name'],
          description: item['description'],
          forks_count: item['forks_count'],
          stargazers_count: item['stargazers_count'],
          # linguagem: is_ruby? ? 'linguagem: ruby': ''
          # created_at: item.order(created_at: :desc);

        }
      end
    end
  end
end
