module SearchRepositoryService
  class << self
    def execute(search_params)
      RestClient.get("#{api}?q=#{search_params}")
    end

    def api
      'https://api.github.com/search/repositories'
    end
  end
end